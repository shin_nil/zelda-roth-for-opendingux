The Legend of Zelda - Return of the Hylian (OpenDingux Version [A320/GCW0])
===========================================================================
![Title Screen](http://4.bp.blogspot.com/-yGCtepMIUD8/TFdNIS9IECI/AAAAAAAAACo/OiUKpQUpWPo/s1600/s14.png)

This is the first game of a trilogy developed by fans. My intention is to build all the three for OpenDingux.

Original code: [Vincent Jouillat](http://www.zeldaroth.fr)
--------------

History / Changelog
--------------------
###V1.3.1 (28/08/2013)
* GCW-Zero alpha release

###V1.3 (09/07/2012)
* OpenDingux release

###V1.2 (19/08/2010)
Improvements / Fixes

* Fixed minor memory leaks, mentioned in the previous version (thanks again hmn).

###v1.1 (08/08/2010)
Improvements

* Finally fixed the random freezing issue (thanks hmn & sebt3 from GP32X.com forum);
* Fixed a major memory leak that caused the system to hang after loading a save;
* Buttons remapped to look more like the SNES version (with minimal code changes).


The buttons are mapped as follows:
----------------------------------
* Start - Confirm;
* Select - Opens menu to save the game;
* A - Run / Lift;
* B - Attack / Read plates / Talk / Open chests (press and hold);
* Y - Use selected item;
* X - Map;
* R + D-pad - Look around.;
* L - Monster encyclopedia (missed in the previous version)


For more information about the game, including Walkthroughs, visit the creators: [http://www.zeldaroth.fr/us/index.php](http://www.zeldaroth.fr/us/index.php)  
For more information about the OpenDingux port, visit my page (portuguese): [http://www.shinnil.blogspot.com.br](http://www.shinnil.blogspot.com.br)  
For more information about OpenDingux (A320), visit [http://www.treewalker.org/opendingux/](http://www.treewalker.org/opendingux/)  
For more information about GCW-Zero, visit [http://www.gcw-zero.com/](http://www.gcw-zero.com/)  
